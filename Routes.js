import React , { Component }from "react";
import { Router, Scene } from "react-native-router-flux";
import ToDo from "./components/ToDo";
import LoginForm from "./components/LoginForm";
import InputTask from "./components/task/InputTask";
import Register from "./components/Register";
import ToDoList from "./components/task/ToDoList";
import Detail from "./components/task/Detail";

export default class Routes extends Component {
   constructor(props) {
     super(props)
   
     this.state = {
        token:'',
        statusLogin:false,
     }
   }
   
  render() {

    return (
      <Router>
        <Scene key="root" hideNavBar>
          <Scene key="LoginForm" component={LoginForm} initial/>
          <Scene key="Register" component={Register} />
          <Scene key="ToDo" component={ToDo} />
          <Scene key="InputTask" component={InputTask} />
          <Scene key="Detail" component={Detail} />
          <Scene key="ToDoList" component={ToDoList} />
        </Scene>
      </Router>
    );
  }
}
